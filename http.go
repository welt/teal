/* HTTP related structs and helpers */

package teal

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/textproto"
	"strings"
)

type loginResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	Session string `json:"session"`
}

type userResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	User    User   `json:"user"`
}

var quoteEscaper = strings.NewReplacer("\\", "\\\\", `"`, "\\\"")

/* createFormFileWithMIME is a modification of multipart.CreateFormFile() that
   allows arbitrary mime-types. */
func createFormFileWithMIME(w *multipart.Writer, fieldname, filename string, mimeType string) (io.Writer, error) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition",
		fmt.Sprintf(`form-data; name="%s"; filename="%s"`,
			escapeQuotes(fieldname), escapeQuotes(filename)))
	h.Set("Content-Type", mimeType)
	return w.CreatePart(h)
}

func escapeQuotes(s string) string {
	return quoteEscaper.Replace(s)
}
