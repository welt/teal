# teal
[![godoc](https://img.shields.io/badge/Documentation-godoc-%23009999)](https://godoc.org/gitea.com/webb/teal) [![pkg.go.dev](https://img.shields.io/badge/Documentation-go.pkg.dev-%23009999)](https://pkg.go.dev/gitea.com/webb/teal)

This is a simple library for pxl.blue, an image hosting site. Consider this library extremely unstable
as pxl.blue and this library are still very early in development.

## Examples

### Logging in, and uploading a file

```
import (
	"fmt"
	"os"
	"gitea.com/webb/teal"
)

func main() {
	client, _ := teal.Login("username", "password")
	file, _ := os.Open("glenda.png")
	image, _ := client.UploadFile(file, "glenda.png")
	fmt.Println(image)
}
```

### Getting an upload key with username and password

```
package main

import (
	"fmt"
	"gitea.com/webb/teal"
)

func main() {
	client, _ := teal.Login("username", "password")
	fmt.Println(client.UploadKey)
}
```

### Manually creating a barebones client and uploading a file

```
import (
	"fmt"
	"os"
	"gitea.com/webb/teal"
)

func main() {
	client := teal.Client{
		UserAgent: "teal",
		UploadKey: "key",
	}

	file, _ := os.Open("glenda.png")
	url, _ := client.UploadFile(file, "glenda.png")
	fmt.Println(url)
}
```
