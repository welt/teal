/* Client related stuff, and its library interfaces */

package teal

// Client represents the client used for making http requests, including the session if used.
type Client struct {
	UploadKey string
	Session   string
	UserAgent string
	User      User
}

// Login will login to an account with the provided credentials and give you a Client with user info back
func Login(username string, password string) (Client, error) {
	s, err := getSession(username, password)
	if err != nil {
		return Client{}, err
	}

	u, err := GetAccountInfo(s)
	if err != nil {
		return Client{}, err
	}
	var client Client

	client.User = u
	client.Session = s
	client.UploadKey = u.UploadKey
	client.UserAgent = "teal"

	return client, nil
}
