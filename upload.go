/* Functions, methods and whatnot for uploading to pxl.blue */

package teal

import (
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"
	"path/filepath"
)

// UploadFile attempts to upload file from io.Reader and returns the response
func (c *Client) UploadFile(reader io.Reader, name string) (response string, error error) {
	rd := reader
	n := name

	r, w := io.Pipe()
	m := multipart.NewWriter(w)

	go func() {
		defer w.Close()
		defer m.Close()
		part, err := createFormFileWithMIME(m, "file", n, mime.TypeByExtension(filepath.Ext(n)))
		if err != nil {
			return
		}
		if err != nil {
			return
		}
		if _, err = io.Copy(part, rd); err != nil {
			return
		}
		m.WriteField("key", c.UploadKey)
	}()

	rs, err := http.Post("https://api.pxl.blue/upload/sharex", m.FormDataContentType(), r)
	if err != nil {
		return "", err
	}
	rb, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		return "", err
	}

	return string(rb), err
}
